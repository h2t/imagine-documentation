#!/bin/bash
sudo apt install curl -y
curl https://packages.humanoids.kit.edu/h2t-key.pub | sudo apt-key add -
echo -e "deb http://packages.humanoids.kit.edu/bionic/main bionic main\ndeb http://packages.humanoids.kit.edu/bionic/testing bionic testing" | sudo tee -a /etc/apt/sources.list.d/armarx.list

sudo apt update
sudo apt install wget h2t-armarx-meta h2t* liblmdb-dev libgsl-dev liburdfdom-dev -y

sudo update-alternatives --install /usr/bin/gcc      gcc      /usr/bin/gcc-8      100
sudo update-alternatives --install /usr/bin/g++      g++      /usr/bin/g++-8      100
sudo update-alternatives --install /usr/bin/gfortran gfortran /usr/bin/gfortran-8 100

# working directory
WS_DIR=$(pwd)
ARMARX_ROOT_DIR="${HOME}/armarx"
mkdir -p "${ARMARX_ROOT_DIR}"

# setup bullet
echo "=========================== setup bullet =================================="
BULLET_DIR="${ARMARX_ROOT_DIR}/bullet3-2.83.7/"
if [ -d "$BULLET_DIR" ] 
then
    echo "bullet exists"
else
    cd $ARMARX_ROOT_DIR
    wget https://github.com/bulletphysics/bullet3/archive/2.83.7.tar.gz
    tar xf 2.83.7.tar.gz
    rm 2.83.7.tar.gz
    mkdir -p bullet3-2.83.7/build
fi

cd $ARMARX_ROOT_DIR/bullet3-2.83.7/build
cmake .. -DCMAKE_INSTALL_PREFIX=${ARMARX_ROOT_DIR}/bullet3-2.83.7/build/install/ -DBUILD_SHARED_LIBS=on -DCMAKE_BUILD_TYPE=Release -DUSE_DOUBLE_PRECISION=on
make -j6
make install


# soem, raw_socket, simox
echo "=========================== clone soem/ImagineRT/Imagine =================================="
cd $ARMARX_ROOT_DIR
if [ ! -d "$ARMARX_ROOT_DIR/soem" ]
then
    echo "clone soem"
    git clone https://github.com/OpenEtherCATsociety/SOEM.git soem
    mkdir -p $ARMARX_ROOT_DIR/soem/build
fi

if [ ! -d "$ARMARX_ROOT_DIR/ImagineRT" ]
then
    echo "clone ImagineRT"
    cd $ARMARX_ROOT_DIR && git clone https://gitlab.com/h2t/ResearchProjects/Imagine/ImagineRT.git
    mkdir -p $ARMARX_ROOT_DIR/ImagineRT/build
fi

if [ ! -d "$ARMARX_ROOT_DIR/Imagine" ]
then
    echo "clone Imagine"
    cd $ARMARX_ROOT_DIR && git clone https://gitlab.com/h2t/ResearchProjects/Imagine/Imagine.git
    mkdir -p $ARMARX_ROOT_DIR/Imagine/build
fi

echo "=========================== setup soem =================================="
cd $ARMARX_ROOT_DIR/soem/build && cmake .. && make -j6 && sudo make install

echo "=========================== setup raw_socket =================================="
cd $ARMARX_ROOT_DIR/ImagineRT/etc/raw_socket && make clean && make && sudo make install

echo "=========================== setup simox =================================="
cd $ARMARX_ROOT_DIR
if [ ! -d "$ARMARX_ROOT_DIR/simox" ]
then
    echo "clone simox"
    git clone https://gitlab.com/Simox/simox.git
fi

cd simox/build
CXX=g++-8 cmake -DBULLET_INCLUDE_DIRS=${ARMARX_ROOT_DIR}/bullet3-2.83.7/build/install/include/bullet/ -DCMAKE_BUILD_TYPE=Release  ..
make -j6


# dmp
echo "=========================== setup DMP =================================="
cd $ARMARX_ROOT_DIR
if [ ! -d "$ARMARX_ROOT_DIR/dmp" ]
then
    echo "clone dmp"
    git clone https://gitlab.com/h2t/DynamicMovementPrimitive.git dmp
    mkdir -p dmp/build
fi

cd dmp/build && cmake .. && make -j6


# ArmarX base packages
echo "=========================== setup armarx base =================================="
cd $ARMARX_ROOT_DIR
if [ ! -d "$ARMARX_ROOT_DIR/armarx" ]
then
    echo "clone armarx"
    git clone https://gitlab.com/ArmarX/armarx.git
fi

cd armarx
git submodule update --init
git submodule foreach 'git checkout master'
 
if [ ! -d "$ARMARX_ROOT_DIR/armarx/ArmarXCore/build/" ]
then
    mkdir -p $ARMARX_ROOT_DIR/armarx/ArmarXCore/build
else
    echo "build directory existing"
fi
cd $ARMARX_ROOT_DIR/armarx/ArmarXCore/build && cmake ..
source $HOME/.bashrc
$ARMARX_ROOT_DIR/armarx/ArmarXCore/build/bin/armarx-dev build RobotSkillTemplates ArmarXSimulation
# echo "export PATH=$PATH:${ARMARX_ROOT_DIR}/armarx/ArmarXCore/build/bin:${ARMARX_ROOT_DIR}/armarx/MemoryX/build/bin" >> $HOME/.bashrc
source $HOME/.bashrc

echo "=========================== setup stable imagine packages =================================="
cd $ARMARX_ROOT_DIR/ImagineRT && git checkout demo
cd $ARMARX_ROOT_DIR/ImagineRT/build && cmake .. && make -j6
cd $ARMARX_ROOT_DIR/Imagine && git checkout demo
cd $ARMARX_ROOT_DIR/Imagine/build && cmake .. && make -j6
 
