alias qt='cd ~/QtCreator/latest/bin && ./qtcreator'
alias sros='if [ -z "$SROS_HAS_RUN" ] ; then source ~/imagine_ws/devel/setup.bash && source ~/cvbridge_build_ws/install/setup.bash --extend && export SROS_HAS_RUN=1; fi'
alias ades='sros && rosrun uibk_libades_ros adesdb --home ~/repo/ades_database_kitgripper'
alias adesm='sros && rosrun uibk_libades_ros motiondb --home /common/homes/staff/imagine-user/repo/ades_database_kitgripper_git/motiondb'
alias ades2='sros && rosrun uibk_libades_ros ades2db --home $HOME/repo/ades_database_kitgripper_git/ades2db/'
alias listades='sros && rosservice call /adesdb/list_ades'
alias as='armarx start'
alias ag='armarx gui'
alias am='armarx memory start'
alias astart='as && am && ag'
alias ap='armarx stop'
alias astop='armarx memory stop && ap'
alias ak='armarx killAll'
alias aki='armarx killIce'
alias kg='pkill -9 ArmarXGuiRun'
alias ra='armarx reset'

# training screw localizer KIT-ArmarX
alias tf2='ana && conda activate tf2'
alias tf='ana && conda activate tf'
alias trainscrew='/common/homes/staff/imagine-user/repo/armarx/Imagine/etc/scripts/convNet.py.in --pos_input_dir=/common/homes/staff/imagine-user/repo/kit_gripper_screw_database/SortedImg_KIT/Screw --neg_input_dir=/common/homes/staff/imagine-user/repo/kit_gripper_screw_database/SortedImg_KIT/NoScrew --is_train --old_model_dir /common/homes/staff/imagine-user/repo/armarx/Imagine/data/Imagine/ScrewLocalizer'


# Git aliases by Rainer Kartmann - let me now if you don't like them
alias gs="git status"
alias gl="git log --all --oneline --decorate --graph"
alias gd="git diff"
alias gg="git gui"
alias gf="git fetch"
alias gfm="git fetch origin master:master"
alias grp="git remote prune origin"
alias gk="gitk --all"

# azure kinect
alias kinect="source ~/Downloads/SetupAzureKinect.sh && azure_kinect_activate"

# singularity
alias sing='singularity shell --nv ~/singularity/iis-robot.sif'


# conda virtual env
alias iv='ana && conda activate imagine-venv'
alias iv2='ana && source $HOME/cvbridge_build_ws/devel/setup.bash --extend && conda activate imagine-venv2'


# nerian
alias nerian='cd ~/repo/nerian-vision-software-8_3_0-src/bin && ./nvcom'


# make
alias mi='MAKEFLAGS="-j6" armarx-dev build Imagine -j6'


# perception nodes
alias screw='source ~/ugoe_env/bin/activate && sros && rosrun ugoe_screw_detection_ros candidate_generator.py'
alias gap='source ~/ugoe_env/bin/activate && sros && rosrun ugoe_gap_detection_ros gap_detector.py'
alias percept='sros && rosrun ros2armarx kit_perception.launch'


