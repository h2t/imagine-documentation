![logo](./images/logo.png)

# Start the Main Loop

some of the useful aliases that you can use
```shell
##### ROS, workspace #####
alias siros='source ~/imagine_ws/devel/setup.bash'
alias sros='if [ -z "$SROS_HAS_RUN" ] ; then source ~/imagine_ws/devel/setup.bash && source ~/cvbridge_build_ws/install/setup.bash --extend && export SROS_HAS_RUN=1; fi'


##### ADES #####
alias ades='siros && rosrun uibk_libades_ros adesdb --home ~/repo/ades_database_kitgripper'
alias ades2='siros && roslaunch ros2armarx launch_ades.launch'
alias listades='siros && rosservice call /ades2db/list_ades'


##### pylon
alias pylon='sros && roslaunch pylon_camera pylon_camera_node.launch'
alias pylon1='ROS_NAMESPACE=/pylon_camera_node rosrun image_proc image_proc'
##### nerian
alias nerian='siros && roslaunch nerian_stereo nerian_stereo.launch'
alias nerian1='cd ~/repo/nerian-vision-software-8_3_0-src/bin && ./nvcom'

#### GUI
alias setup='siros && roslaunch ros2armarx kit_setup.launch'
alias krviz='siros && roslaunch ros2armarx kit_rviz.launch'

##### conda virtual env
alias ivt='source ~/ugoe_env_test/bin/activate && sros'


##### ASC, planner, phys
alias asc='siros && roslaunch imagine_asc imagine_asc.launch'
alias plan='siros && roslaunch csic_imagine_planner planner_bringup.launch'
alias phys='siros && roslaunch phys_pkg physServer.launch'

##### MAIN LOOP #####
alias kloop='siros && rosrun imagine_review2 review_loop.py'
alias ksetup='siros && roslaunch ros2armarx kit_demo_setup.launch'
alias kros='siros && roslaunch ros2armarx ros_components.launch'
alias kpercept='ivt && sros && roslaunch ugoe_imagine_ros ugoe_imagine_ros.launch'
alias kphys='siros && rosrun phys_registration registration_rosserv /common/homes/staff/imagine-user/imagine_ws/setup_IMAGINE/registration_db/fake_test_phys.txt'
alias ktrigger='siros && rostopic pub /archi/trigger std_msgs/Bool "data: true"'
alias kcache='iv && sros && rosrun imagine_plumbing services_cache.py'
alias kcacheclean='siros && rosservice call /perception/cached/analyze_scene/clear'

#### Final Demo
alias kdemo='siros && roslaunch ros2armarx kit_demo_ros_all.launch'
alias k86='ssh imagine-user@i61pc086'
alias kwire='source .bashrc_wiredetect && ivt && siros && roslaunch ugoe_imagine_ros kit_wire_component.launch'
```


# Start A Disassembly Task in Fully Autonomous Mode

## Compact launching script

- First of all, launch all ROS components:

    ```shell
    # on i61imagine-demo PC, this will launch cameras, asc, planner, phys, plumbing, etc
    kdemo
    
    # on i61imagine-demo PC, this will launch screw detection and perception wrapper
    kpercept

    # ssh to pc086
    k86
    # and then run the following for wire detectiona and component segmentation
    kwire
    ```

    Note that: To use another PC for wire detection and component segmentation, you need to ssh to that PC, run `ifconfig` to get the IP address (ipv4) and copy / past it to the entry `export ROS_IP=xxx` in the bashrc file `.bashrc_wiredetect`. This allows your bash environment to use the i61imagine-demo PC as ros master and this pc as slave/client, in the meanwhile using the same account to share files.
    
- secondly, run ArmarX and start scenarios

    ```shell
    as && ag
    # switch to the corresponding GUI layout
    ```
    
- Then, start the main loop

    ```shell
    kloopa
    ```
    
- Finally, trigger the main loop: go to RemoteGUI of ROSTaskProvider in ArmarXGUI and click the `StartDemo` button. You can also trigger screw detection with `DetectScrews` button.
    


## Step-by-step procedure

1. start a terminal and run **ROS master**: with `roscore`
2. start the **ADES and motion database** with `ades2`
3. launch the **Pylon RGB camera** by `pylon && pylon1` and the **stereo camera** by `nerian`.
4. **Perception Node**: Now we setup a virtual env `iv` and new workspace with python3 `cv-bridge`. And then <u>bring up the perception node by</u> `kpercept`, which will launch the perception wrapper and screw detection service. To reduce the computation load, we'd like to <u>run the other perception tasks (wire detection, component segmentation) on another PC by</u> `kwire`. The communication is managed by ROS.
5. run **ASC** by `asc`.
6. run **planner** by `plan`.
7. run **phys simulation** by `phys`.
8. **GUI** for ROS components: `setup && krviz`.
9. Start ArmarX by `as && ag` and run the scenarios, and choose a GUI layout as prefer.
10. Run the **main loop** by `kloop`: this script will stop and wait for the user to start each components.
11. trigger to start the process by `ktrigger`.

### Note

***Cameras***
    
1. Pylon
    
    You can also visualize the images or streams using the following software
    ```shell
    /opt/pylon/bin/pylonviewer
    ```
    
    In the `acquisition control`, you can turn off the 'trigger software' (trigger mode: "off"). The pylon node will change this setting to "on" again when you launch it.

2. Stereo: it will download the calibration file from the camera to local machine and read this configuration file.
    
    you can also try the app under 
    ```shell
    cd ~/repo/nerian-vision-software-8_3_0-src && ./bin/nvcom
    ```
    or use the web based interface

3. Camera pose should be calibrated and the transform information is put into the URDF file

    ```
    /common/homes/staff/imagine-user/imagine_ws/src/imagine_ros2aramrx/urdf/kit_setup.xacro
        ```

***Perception Nodes***

- to trigger the screw detection, you can run
    
    ```shell
    rosservice call /ugoe_screw_detection_ros/detect_screws "{}"
    ```
    
- to test the wire detection offline, you can run the following:
    
    ```shell
    iv
    sros
    cd ~/imagine_ws/src/imagine_state_estimation/ugoe_wire_detection
    python wire_detection.py -i ./wire.png --working_dir /tmp 

    cd ~/imagine_ws/src/imagine_state_estimation/ugoe_component_segmentation
    python component.py -i ./image.png --working_dir /tmp
    ```
    
    there will be a component map generated in `~/tmp`
            
    ```shell
    # call a service provided by perception nodes
    rosservice call /perception/analyze_scene
    ```
        

***Planner***

to launch examples 4 and 5 (this is not required by the main loop
    
```shell
roslaunch csic_imagine_planner usage_example.launch example:=4
roslaunch csic_imagine_planner usage_example.launch example:=5
```

***PHYS simulation***

1. bringup sofa physServer which privides the service

```shell
siros
roslaunch phys_pkg physServer.launch
```

2. test phys

    go to the following working_dir
    
    ```shell
    cd ~/sofaserver/workdir_sofaserver/ZI620
    /common/homes/staff/imagine-user/repo/sofa/build/bin/runSofa scene.py
    ```

2. run the registration

    ```shell
    siros
    rosrun phys_registration registration_rosserv /common/homes/staff/imagine-user/imagine_ws/s/imagine_phys/hdd_models/export_mesh/hitachi_deskstar_S1_Q0.001/
    ```

    <u>using all models</u>: Coming soon

3. Other instructions:

    ```shell
    roslaunch phys_pkg standaloneExample.launch
    ```

    1. change the toml configuration file and pass the path to physConfig in imagine_phys/ROS_phys/launch/physServer.launch
    2. launch physServer, it will wait for the service call to start a simulation.
    ```shell
    roslaunch phys_pkg physServer.launch
    ```

    - the config file is here: /common/homes/staff/imagine-user/imagine_ws/config/phys-config.toml
    I added the path in this launch file: /common/homes/staff/imagine-user/imagine_ws/src/imagine_phys/ROS_phys/launch/standaloneExample.launch
    - results: a gui pop up without anything else

    - references:
    - example of config: https://github.com/IMAGINE-H2020/imagine_phys/blob/master/ROS_phys/phys-config.toml.sample
    - hdd_models: https://mybox.inria.fr/f/db28100cd30542cf92f8/?dl=1 download the mesh files

***main loop***
 This will send boolean message "arch_trigger" message to the topic. if auto is False, it will run every step once and go back to the start point. If auto is True, it will continue steps without waiting.

# Start A Predefined Disassembly Task

We will demonstrate how to start a disassembly task using one of the reference hard-disc devices (HDDs). 

- Download the `ros2armarx` package. It includes example configuration files with predefined action sequences and corresponding parameters of each actions.
- Compile the ROS workspace
- Configure your own configuration file, e.g. `hdd1.yaml`
- Run the following scripts

```shell
rosrun ros2armarx execute.py hdd1
```


# Start The Online-Teaching Process
coming soon

# Parts tracking

1. start the nerian nodes and start ArmarX GUI. 
2. Place a Aruco marker on the gripper jaw and 
3. Start the `ImagineExternalCameraCalibration` scenario. Start a **new image viewer** to visualize the coordinate frame of the Aruco markers. 
4. Run an action (e.g. Lever) and start `GripperLogging` Component. Make sure you enable the feature `enableArMarkerRecording`

