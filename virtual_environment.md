![logo](./images/logo.png)


# Setup Python Virtual Environment

We need a virtual environment to run the python node of Screw Detection and Classification, as well as scripts used for the model-retraining GUI. We decided to use Conda environment since the native python virtual environment may have a conflict version of Numpy with the required one.

## Setup Anaconda

Follow the [official instructions](https://docs.anaconda.com/anaconda/install/).

## Setup a virtual environment and install dependencies

to prevent ROS from using python executables from Anaconda, we have to wrap the conda environment settings into a function in `.bashrc`. Here is an example, you can also give if a name alias for quick access.

```shell
function condahelper(){
    # !! Contents within this block are managed by 'conda init' !!
    __conda_setup="$('/common/homes/staff/imagine-user/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
    if [ $? -eq 0 ]; then
        eval "$__conda_setup"
    else
        if [ -f "/common/homes/staff/imagine-user/anaconda3/etc/profile.d/conda.sh" ]; then
            . "/common/homes/staff/imagine-user/anaconda3/etc/profile.d/conda.sh"
        else
            export PATH="/common/homes/staff/imagine-user/anaconda3/bin:$PATH"
        fi
    fi
    unset __conda_setup
    # <<< conda initialize <<<
}

alias ana=condahelper
```

Open a terminal and execute

```shell
ana
conda create -n imagine-venv python=3.6
```

## Setup virtual environment for state estimation

pip3 install -U pip

python3 -m venv ugoe_env
. ~/ugoe_env/bin/activate
pip3 install -r ~/imagine_ws/src/imagine_state_estimation/requirements.txt 

sros

rosrun ugoe_screw_detection_ros candidate_generator.py 



## Setup the requirement for Screw Training GUI

1. Clone the following repository which contains adapted scripts for *preparing the data, uploading it to google collab, downloading the trained weights and some utility needed for the gui*: https://github.com/sreither/ugoe_screw_detection_google_collab_training_scripts.git

    - They are based on the original scripts from here: https://github.com/hamzaleroi/dnn_migration

2. Follow the instructions in the `Readme.md` of the clone repository

3. Adapt the configuration file `CommandsAndPaths.json according to the paths for your setup.

4. Login in to the used google account for the first time (imagine.horizon2020@gmail.com, password: Swtor3535*). This will result in an two-factor-authentification request to Eren's mobile phone. Send him an E-Mail so that he can send you the code.
