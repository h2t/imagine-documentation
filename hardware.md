![logo](./images/logo.png)

# Hardware Setup

![imagine_hardware_all](./images/hardware_all.png)

System setup of the Milestone-4 demonstrator. The steup is similar to the one used for MS3, but located at KIT. The yellow ellipsoids mark the (a) **the RGB camera** and (b) **the stereo camera**. (c) is **the KIT Multifunctional Gripper** disassembling one of the reference HDDs. (d) plays the role of a **conveyor belt** for transporting a new device to the IMAGINE system and moving the disassembled parts away. The robot is equipped with multiple **disassembly tools** (f) in **the tool magazine** (e), including from left to right 4 screwdrivers of type *Torx6, SDFlat, SDPH1, Torx8*, the lever tool and 2 cutting tools by rotating and slicing respectively.


![imagine_hardware](./images/cable_connection.png)

# Datasheet

<a href="https://h2t.gitlab.io/imagine-documentation/hardware_doc/imagine_datasheet.pdf" style="text-align:center; background: url('https://h2t.gitlab.io/imagine-documentation/images/imagine_datasheet.png');" download>Click to Download imagine_datasheet.pdf</a>

# Camera Calibration

To visualize the model used for robot calibration, use the following
```shell
${Simox_DIR}/build/bin/RobotViewer --robot ~/repo/armarx/ImagineRT/data/ImagineRT/robotmodel/KITGripper/KITGripper_aruco.xml
```

Select the reference coordinate system, either the base of the robot or the camera frame:

![calibration_panel](./images/armarker_calibration_arviz_panel.png)

you should adjust the size of the marker in the remote gui panel `ArMarkerLocalizerOpenCV`:

![calibration_panel](./images/marker_size.png)
